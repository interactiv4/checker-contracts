<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\Checker\Api;

use Interactiv4\Contracts\DataObject\Api\DataObjectInterface;

/**
 * Interface CheckerInterface.
 *
 * @api
 */
interface CheckerInterface
{
    /**
     * Performs some check, optionally using supplied data,
     * and returns boolean value according to check performed.
     *
     * @param DataObjectInterface|null $data
     *
     * @return bool true if check was successful, false otherwise
     */
    public function check(DataObjectInterface $data = null): bool;
}
