<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\Contracts\Checker\Api;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;

/**
 * Interface CheckerInterfaceFactoryInterface.
 *
 * @api
 */
interface CheckerInterfaceFactoryInterface extends FactoryInterface
{
    /**
     * {@inheritdoc}
     *
     * @return CheckerInterface
     */
    public function create(array $arguments = []): CheckerInterface;
}
